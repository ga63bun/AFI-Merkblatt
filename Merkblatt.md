---
geometry: margin=0.2cm
papersize: a4
documentclass: extarticle
fontsize: 8pt
---


# Grundlagen

**Dreiecksungleichug:** $|a + b| \leq |a| + |b|$

**Umgekehrte Dreiecksgleichung:** $||x| - |y|| \leq |x + y|$

**Schranken, Supremum, Infimum:**

* $s \in K$ \textbf{obere Schranke} von $X$: $\forall x \in X: s \geq x$.
* $s \in K$ \textbf{Supremum} von $X$: $s$ kleinste obere Schranke/ $\forall x \in X: s \geq x \land \forall y \in K, \text{y obere Schranke von } X: s \leq y$
* $s \in K$ \textbf{Maximum} von $X$: $s = sup(X) \land s \in X$.

**$\varepsilon$-Char. d. Supremums:** $(K,+,\cdot) \text{ angeordn. Körper: } X \subset K, X \neq \emptyset, s \text{ o.Schr. von } X: s = sup(X) \Leftrightarrow \forall \varepsilon > 0: \exists x \in X: s- \varepsilon < x \leq s$

**Supremumseigenschaft:** Ang. Körper $(K, +, \cdot)$ \textbf{vollständig}, wenn Supr.eig.: $\forall X \subset K \text{ nichtleer, nach oben beschränkt }: \exists s = sup(X) \in K$

**Archimedische Anordnung, Dichtheit von $\mathbb{Q}$ in $\mathbb{R}$:**

- $\mathbb{R}$ ist archimedisch angeordneter $\Rightarrow$ $\forall x \in \mathbb{R} : \exists n \in \mathbb{N} : n > x$
- $\forall \varepsilon > 0: \exists n \in \mathbb{Q} : \frac{1}{n} < \varepsilon$ (,,$\mathbb{Q}$ dicht in $\mathbb{R}$'')
- $\forall a,b \in \mathbb{R} a < b : \exists q \in Q : a < q < b$


**Bewiesene Ableitungen**

\begin{supertabular}{r|l}
$F(x)$					& $f(x)$							\tabularnewline
\hline
$(ax)$ &				$ a$							\tabularnewline

$(x^n)$					& $nx^{n-1}$					\tabularnewline

$(\ln(|x|))$			& $\frac{1}{x}$					\tabularnewline

$(\frac{1}{x})$			& $- \frac{1}{x^2}$				\tabularnewline

$(e^x)$					& $e^x$							\tabularnewline

$(a^x)$					& $a^x \cdot \ln(a)$			\tabularnewline

$(x \cdot \ln(x) - x)$	& $\ln(x)$						\tabularnewline

$\tan(x)$				& $\frac{1}{cos^2(x)}$/ $1+\tan^2(x)$			\tabularnewline

$\ln(\sin(t))$			& $\cot(t)$						\tabularnewline

$\sinh(x)$				& $\cosh(x)$					\tabularnewline

$\cosh(x)$				& $\sinh(x)$					\tabularnewline

$\arcsin(z)$			& $\frac{1}{\sqrt{1 - z^2}}$	\tabularnewline

$\arccos(z)$			& $\frac{-1}{\sqrt{1 - z^2}}$	\tabularnewline

$\arctan(z)$			& $\frac{1}{1 + z^2}$			\tabularnewline

\end{supertabular}

**Trigonometrie**

$\cos(z) := \sum_{k=0}^{\infty} (-1)^k\frac{z^{2k}}{(2k)!} = \frac{e^{iz} + e^{-iz}}{2}$

$\sin(z) := \sum_{k=0}^{\infty} (-1)^k\frac{z^{2k+1}}{(2k+1)!} = \frac{e^{iz} - e^{-iz}}{2i}$

$\tan(z) := \frac{\sin(z)}{\cos(z)}$, $\tan(z) = \sum_{k=0}^{\infty} (-1)^k \frac{x^{2k+1}}{2k+1}$

$\cot(z) := \frac{\cos(z)}{\sin(z)}$

$\sin^2(x) + \cos^2(x) = 1$

$\sin(z+w) = \sin(z)\cos(w) + \cos(z)\sin(w)$

$\cos(z+w) = \cos(z)\cos(w) - \sin(z)\sin(w)$

$\cos(2z) = \cos(z)^2 - \sin(z)^2$

$\sin(2z) = 2\sin(z)\cos(z)$

$\sin^2(x) = \frac{1}{2} (1 - \cos(2x))$

$\cos^2(x) = \frac{1}{2} (\cos(2x) + 1)$

$\sin(x)\cos(x) = \frac{1}{2} \sin(2x)$

$\cos(k\pi) = (-1)^k$

$\sin(x) = O(x) \land \sin(x) = x + O(x^3)$ für $x \rightarrow 0$.

\begin{supertabular}{r|ccccc}
$x$ &		$0$ &	$\frac{\pi}{6}$ &		$\frac{\pi}{4}$ &		$\frac{\pi}{3}$ &		$\frac{\pi}{2}$ \tabularnewline
\hline
$sin(x)$ &	$0$ &	$\frac{1}{2}$ &			$\frac{\sqrt{2}}{2}$ &	$\frac{\sqrt{2}}{3}$ &	$1$				\tabularnewline
$cos(x)$ &	$1$ &	$\frac{\sqrt{2}}{3}$ &	$\frac{\sqrt{2}}{2}$ &	$\frac{1}{2}$ &			$0$				\tabularnewline
\end{supertabular}


**Hyperbolische Funktionen**

Eine Hyperbel ist die Kurve mit der Spur

$H(t) = (\cosh(t), \sinh(t))$


$\tanh : \mathbb{R} \rightarrow \mathbb{R}$ bijektiv

$\cosh(z) = \frac{e^z + e^{-z}}{2} = \sum_{k=0}^{\infty} \frac{z^{2k}}{(2k)!}$

$\sinh(z) = \frac{e^z - e^{-z}}{2} = \sum_{k=0}^{\infty} \frac{z^{2k+1}}{(2k+1)!}$

$\tanh(z) := \frac{\sinh(z)}{\cosh(z)}$

$e^z = \sinh(z) + \cosh(z)$

$\sinh(z+w) = \sinh(z)\cosh(w) + \cosh(z)\sinh(w)$

$\cosh(z+w) = \cosh(z)\cosh(w) - \sinh(z)\sinh(w)$

$\sinh^2(x) - \cosh^2(x) = 1$


**Exponentialfunktion**

$\exp(z) := \sum_{k=0}^{\infty} \frac{z^k}{k!} = \lim_{n\rightarrow\infty}(1+\frac{1}{n})^{nz}$

$(e^t)' = e^t = \int e^t dt$

$e^a \cdot a^b = e^{a+b}$, $e^{-z} = \frac{1}{z}$, $e^{z} \neq 0$, $e^{\overline{z}} = \overline{e^z}$

$e^{z+2\pi i} = e^z$

$\lim_{x \rightarrow 0} \frac{e^x - 1}{x} = 1$, $\forall n \in \mathbb{N}: \lim_{x \rightarrow \infty} x^{-n}e^x = \infty \land \lim_{x \rightarrow -\infty} x^ne^x = 0$

$|exp(z) - \sum_{k=0}^{n} \frac{z^k}{k!}| \leq 2 \cdot \frac{|z|^{n+1}}{(n+1)!}$

$a^z = e^{z \cdot \ln(a)}$, $\forall x \in \mathbb{R}: |e^{ix}| = 1$

**Logarithmus**

$ln(x) := exp^{-1}(x)$

$ln(x) + ln(y) = ln(x \cdot y)$,
$ln(x) - ln(y) = ln(\frac{x}{y})$,
$ln(x^n) = n \cdot ln(x)$,
$ln(\sqrt[n]{x}) = \frac{1}{n} \cdot ln(x)$

$\lim_{x \rightarrow 0} \frac{\ln(1+x)}{x} = 1$

$\ln(1+x) = \sum_{k = 1}^{\infty} (-1)^{k+1} \frac{x^k}{k}, x \in (-1, 1]$

**Paritalbruchzerlegung**

Zum Zerlegen von Bruch von Polynomen $\frac{P}{(x+a) \cdot (x+b)}$

- Schreibe $\frac{A}{(x+a)} + \frac{B}{(x+b)}$
- Löse $A \cdot (x+b) + B \cdot (x+a) = P$ durch einsetzen von jewils $x = -a$ bzw $x = -b$

**Lineare Algebra**

$I_n =$ Matrix mit 1 auf der Diagonale, z.B. $\begin{pmatrix}1 & 0 \\ 0 & 1\end{pmatrix}$

$det\begin{pmatrix} a & b \\ c & d\end{pmatrix} = ad - bc$

$\chi_A(\lambda) = det(A-\lambda I_n)$

Eigenwerte $\lambda$ durch $\chi_A(\lambda) = 0$





# Grenzwerte/Folgen

Hat folge $a_n$ einen GW, dann ist sie konvergent und deswegen *beschränkt*. Es gibt nur einen GW.

$a \in \mathbb{R}$ GW von $(a_n)$, wenn $\forall \varepsilon > 0 : \exists n_0 \in \mathbb{N} : \forall n \geq n_0 : | a_n - a| < \varepsilon$

**Tipp:**
$|a_n - a| < \varepsilon \Leftrightarrow a - \varepsilon < a_n < a + \varepsilon$

**Mehrdimensional/ Komplex:** Jede Komponente konvergiert/ $\lim_{n \rightarrow \infty} ||v_n -v||_2 = 0$

**Bewiesene Grenzwerte:**

- $\frac{1}{n^p} \rightarrow 0$ (VL)
- $\frac{1}{\sqrt{n}} \rightarrow 0$ (ÜBUNG)
- $x^n$ mit $0 < x < 1$ $\rightarrow 0$
- $z^n$ mit $\vert z \vert < 1$ $\rightarrow 0$
- $(1 + \frac{1}{n})^n \rightarrow e$
- $\frac{n}{2n+1} \rightarrow \frac{1}{2}$ (VL)
- $\sqrt[n]{n} \rightarrow 1$
- $a_0 = 1, a_{n+1} = \frac{1}{2}(a_n + \frac{2}{a_n}) \rightarrow \sqrt{2}$ (rek.)
- $n^2 \rightarrow \infty$
- $x^n$ mit $x > 1$ $\rightarrow \infty$
- $\frac{n^2 - 1}{n} \rightarrow \infty$
- $(-1)^n$ div. (Hp $-1$, $1$)
- $i^n$ div. (Hp $1, i, -1, -i$)
- $x^n$ mit $x < -1$ div.

**Rechenregeln für Grenzwerte:**

- Die Folge $(a_n + b_n)$ konvergiert gegen $a + b$
- Die Folge $(a_n \cdot b_n)$ konvergiert gegen $a \cdot b$
- Die Folge $(\frac{a_n}{b_n})$ konvergiert gegen $\frac{a}{b}$ wenn $b \neq 0$
- Falls $\forall n : a_n \leq b_n$ dann $a \leq b$
- **Einschließungskrit:** Falls $a = b$ und $(c_n)$ reelle Folge und $a_n \leq c_n \leq b_n$ für fast alle $n \in \mathbb{N}$ $\Rightarrow$ $(c_n)$ konvergiert gegen $a$.

**Eigenschaften konv. Folgen:** $(a_n)$ konvergente Folge **1)** $\Rightarrow$ $(a_n)$ beschränkt (d.h. Menge $\{a_n : n \in \mathbb{N}\}$ beschr.) **2)** $\Rightarrow$ Besitzt genau einen GW.

**Uneigentliche Konvergenz:**
$\forall K > 0 : \exists n_0 \in \mathbb{N} : \forall n \geq n_o : a_n > K$

**Rechenregeln für uneigentliche Konvergenz:**
$(b_n)$ konvergiert gegen $\infty$

- Wenn $a \neq - \infty$ konvergiert $(a_n + b_n)$ gegen $\infty$
- Wenn $a \neq 0$ konvergiert $(a_n \cdot b_n)$ gegen $\infty$
- Wenn $a \notin \{-\infty,\infty\}$ konvergiert $(\frac{a_n}{b_n})$ gegen 0

**Monotonie:**

- *monoton wachsend/fallend* $a_{n+1} \geq a_n$ bzw. $a_{n+1} \leq a_n$
- *streng monoton wachsend/fallend* $a_{n+1} > a_n$ bzw. $a_{n+1} < a_n$

**Monotoniesatz:**
Ist $(a_n)$ eine reelle, monoton wachsende, nach oben beschränkte Folge. Dann ist sie Konvergent mit

$\lim_{n\rightarrow\infty} a_n = \sup\limits_{n \in \mathbb{N}} a_n$

**Teilfolge:**
Man setzt eine streng monoton wachsende Folge für Indices ein.
Sei $(n_k)$ SMW, dann ist $(a_{n_k})$ eine *Teilfolge* von $(a_n)$

**Häufungspunkt:**
$a \in \mathbb{R}$ ist *Häufungspunkt* von $(a_n)$ wenn es eine Teilfolge gibt die gegen $a$ konvergiert

**Satz von Bolzano-Weierstraß:** Jede beschränkte reelle Folge besitzt eine Konvergente Teilfolge und hat deswegen mindestens einen Häufungspunkt

**Limes Superior:** Größter Häufungspunkt

**Limes Inferior:** Kleinster Häufungspunkt





# Reihen

Eine Reihe ist die Summe einer Folge

$s_n := \sum_{k=1}^{n} a_k = a_1 + ... + a_n$

**Konvergenz/Divergenz** auf Folge $(s_n)$ definiert

**Absolut Konvergent** wenn $\sum_{k=1}^{\infty} |a_k|$ konvergiert. Abs. konv. $\Rightarrow$ konv.

**Bewiesene Reihen**

- für $|z| < 1$: $\sum_{k=0}^{\infty} z^k = \frac{1}{1-z}$ (geom. Summenformel) *(Geometrische Reihe)*
- $\sum_{k=1}^{\infty} \frac{1}{k(k+1)}$ konv. $= 1$ (Teleskoprh.)
- $\sum_{k=0}^{\infty} \frac{1}{k!}$ konv. $= e$
- $\sum_{k=0}^{\infty} \frac{1}{k^s}, s \in \mathbb{Q}, s \geq 2$ konv.
- $\sum_{k=0}^{\infty} \frac{1}{k!}$ konv. $= e$
- $\sum_{k=0}^{\infty} \frac{z^k}{k!}$ konv. *(Exponentialreihe)*
- $\sum_{k=1}^{\infty} \frac{(-1)^{k-1}}{k}$ konv. *(altern. harmon. Reihe)*
- $\sum_{k=0}^{\infty} \frac{1}{k}$ div. *(Harmonische Reihe)*
- für $|z| \geq 1$: $\sum_{k=0}^{\infty} z^k$ div

**Teleskopreihe** wenn sich aus jedem Mal zwei Summanden bilden die sich mit dem nächsten Aufheben

**Notw. Bed. f. Konv.:** Folge der Summanden muss gegen $0$ konv.

**Majoranten-/Minorantenkriterium** Sei $|a_k| \leq b_k$ für fast alle $k \in \mathbb{N} \Rightarrow$

- $\sum_{k=0}^{\infty} b_k$ konv $\Rightarrow$ $\sum_{k=0}^{\infty} a_k$ konv abs
- $\sum_{k=0}^{\infty} a_k$ div $\Rightarrow$ $\sum_{k=0}^{\infty} b_k$ div

**Quotientenkriterium:** Sei $q := \lim_{n\rightarrow\infty}|\frac{a_{n+1}}{a_n}|$ ($a_k \neq 0$ fast alle $k$).

- Falls $q < 1$ konvergiert die Reihe absolut
- Falls $q > 1$ divergiert die Reihe

**Wurzelkriterium:** $a_k \in \mathbb{C}$ und $q := \limsup_{k\rightarrow\infty} \sqrt[k]{|a_k|}$

- Falls $q < 1$ konvergiert die Reihe absolut
- Falls $q > 1$ divergiert die Reihe

**Leibniz-Kriterium:** Sei $a_k$ eine relle, monoton fallende Nullfolge. Dann konvergiert $\sum_{k=0}^{\infty} (-1)^k a_k$. $\forall n \in \mathbb{N}:$ $|\sum_{k=0}^{\infty} (-1)^k a_k - s_n| \leq a_{n+1}$.

**Int.vgl.krit.:** $f: [a, \infty) \rightarrow \mathbb{R}$ mon fall, $\forall x \in [a, \infty ): f(x) \geq 0$: $\sum_{k=a}^{\infty} f(k)$ konv $\Leftrightarrow f$ über $[a, \infty)$ uneig intbar.

**Umordnungssatz:** **1)** Reihe abs. konv. $\Rightarrow$ Umordnung konv. geg. selben Wert. **2)** Reihe (reelle Summanden) konv. (nicht abs.) $\Rightarrow$ $\forall s \in \mathbb{R}: \exists$ Permutation: Umordnung konv. gegen $s$.

**Produkt**

$(\sum_{k=0}^{\infty} a_k)(\sum_{k=0}^{\infty} b_k) = (\sum_{m=0}^{\infty} (\sum_{k=0}^{m} a_k b_{m-k}))$



**Potenzreihen:** Eine Reihe $\sum_{k=0}^{\infty} c_k z^k$ mit $c_k, z \in \mathbb{C}$

Konv wenn $|z| < R$ mit $R := \frac{1}{\limsup_{k\rightarrow\infty}\sqrt[k]{|c_k|}}$ ($\frac{1}{0} = \infty$, $\frac{1}{\infty} = 0$) *(Konvergenzradius)*. Div wenn $|z| > R$. Unbek. für $|z| = R$.

PR mit KR $R$: $f:\{z : |z| < R\} \rightarrow \mathbb{C}$ stetig.

**Produkt**

$(\sum_{k=0}^{\infty} a_k z^k)(\sum_{k=0}^{\infty} b_k z^k) = (\sum_{m=0}^{\infty} (\sum_{k=0}^{m} a_k b_{m-k}) z^m)$

Und der Konvergenzradius ist $min\{R_a,R_b\}$

**Division**

$\frac{(\sum_{k=0}^{\infty} a_k z^k)}{(\sum_{k=0}^{\infty} b_k z^k)} = (\sum_{k=0}^{\infty} \frac{1}{b_0}(a_k - \sum_{j=0}^{k-1} c_j b_{k-j}) z^k)$

*(Rekursiv!)* Die Herleitung funktioniert durch Koeffizienten Vergleich: man schreibt $\frac{f(x)}{g(x)} = c(x) \Leftrightarrow f(x) = c(x) \cdot g(x)$ und kann auf $a_x = ...$ auflösen indem man rechts die Formel für Multiplikation einsetzt. Dann Koeffizientenvergleich machen.

**Ableitung**

$(\sum_{k=0}^{\infty} c_k x^k)' = \sum_{k=0}^{\infty} k c_k x^{k-1}$

**Integration**

$\int \sum_{k=0}^{\infty} c_k x^k dx = \sum_{k=0}^{\infty} \frac{1}{k+1} c_k x^{k+1}$




# Stetigkeit

**Def:** $f:D\rightarrow\mathbb{R}$ stetig in $c \in D: \forall (x_n) \text{ in } D$, $\lim_{n \rightarrow \infty} x_n = c: \lim_{n \rightarrow \infty} f(x_n) = f(c)$.**/**
$\lim_{n \rightarrow \infty} = c \Rightarrow$ $\lim_{n \rightarrow \infty} f(x_n) = f(\lim_{n \rightarrow \infty} x_n) = f(x)$.

**$\varepsilon$-$\delta$-Charakterisierung**

$\forall \varepsilon > 0: \exists \delta > 0 : \forall  x: |x-c| < \delta \Rightarrow |f(x) - f(c)| < \varepsilon$

**Rechenregeln** Stetigkeit bleibt durch $f+g$, $f \cdot g$, $\frac{f}{g}$ und $f \circ g$ erhalten

**Bekannte stetige Funktionen: ** Konstanten, Betragsf, Polynome, $\frac{1}{x}$.

**Zwischenwertsatz:** Sei $f$ auf $[a,b]$ stetig $\Rightarrow$ $\forall y \in \mathbb{R}, \min(f(a), f(b)) \leq y \leq \max(f(a), f(b)): \exists x \in [a,b]: f(x) = y$.

**Min/Max Satz:** $f:[a,b] \rightarrow \mathbb{R}$ stetig: **1)** $\Rightarrow$ $f$ beschränkt. **2)** $\Rightarrow$ $\exists x_{max}, x_{min} \in [a,b]: f(x_{max}) = \sup\{f(x) : x \in [a,b]\} \land f(x_{min}) = \inf\{f(x) : x \in [a,b]\}$.

**Funktionsgws:** **1)** Abschluss von $D \subseteq \mathbb{R}: \overline{D} := \{x \in \mathbb{R}: \exists (x_n)$ mit $x_n \in D \land \lim_{n \rightarrow \infty} x_n = x\}$**/** $D$ abgeschlossen $\Rightarrow D = \overline{D}$.
**2)** Sei $D \subseteq \mathbb{R}, f: D \rightarrow \mathbb{R}, c \in \overline{D}: y \in \mathbb{R} \cup \{\pm\infty\}$ GW von $f$ in $c$, wenn $\forall (x_n)$ in $D$ mit $\lim_{n \rightarrow \infty} x_n = c: y = \lim_{n \rightarrow \infty} f(x_n)$. **3)** GW von oben/unten: Zus. $x_n > c$/ $x_n < c$.

**Lipschitz Stetigkeit**

<!--- Beschränkte Änderungsrate -->

$\exists L: \forall x_1, x_2: |f(x_1)-f(x_2)| \leq L \cdot |x_1 - x_2|$





# Differentation

$\lim_{x \rightarrow c} \frac{f(x) - f(c)}{x - c} =: f'(c)$

**Diffbar**: Ableitung existiert in (jedem) Punkt.

**Stetig diffbar**: Ableitung existiert in jedem Punkt und ist stetig.

**Stetig:** f differenzierbar $\Rightarrow$ f stetig.

**Umkehrfunktionen differenzieren:** $(f^{-1})'(f(c)) = \frac{1}{f'(c)}$

**Kettenregel:** $(g \circ f)'(c) = g'(f(c))\cdot f'(c)$.

**Produktregel:** $(f \cdot g)'(c) = f'(c)g(c) + f(c)g'(c)$
**Quotientenregel:** $(\frac{f}{g})'(c) = \frac{g(c)f'(c) - f(c)g'(c)}{g(c)^2}$

**Monotonie**

- mon wachs $\Leftrightarrow \forall x, \widetilde{x}, x < \widetilde{x}: f(x) \leq f(\widetilde{x})$
- str mon wachs $\Leftrightarrow \forall x, \widetilde{x}, x < \widetilde{x}: f(x) < f(\widetilde{x})$

$I \subseteq \mathbb{R}$ Intervall(!), $f: I \rightarrow J \subseteq \mathbb{R}$ stetig: **1)** $f$ inj $\Leftrightarrow$ $f$ str mon wachs/fall **2)** $f$ zusätzl surj $\Rightarrow$ $f^{-1}$ stetig $\land$ mon wachs/fall.

- $\forall x \in I: f'(x) \geq 0 \Leftrightarrow f$ mon wachs
- $\forall x \in I: f'(x) > 0 \Rightarrow f$ str mon wachs

**Extrema**

- *lok Min* $\Leftrightarrow \exists \varepsilon > 0: \forall x \in (c-\varepsilon, c+ \varepsilon)\cap I: f(c) \leq f(x)$

- *iso lok Min* $\Leftrightarrow \exists \varepsilon > 0: \forall x \in (c-\varepsilon, c+ \varepsilon)\cap I: f(c) < f(x)$

- *glob Min* $\Leftrightarrow \forall x \in I: f(c) \leq f(x)$

Extremum in $c$ $\Rightarrow$ $f'(c) = 0$

**Satz v. Rolle:** $f:[a,b] \rightarrow \mathbb{R}$ diffbare Funktion mit $f(a) = f(b)$ $\Rightarrow \exists \xi \in (a,b): f'(\xi) = 0$

**Mittelwerts. der Diff.rech.:** $f:[a,b] \rightarrow \mathbb{R}$ diffbare Funktion $\Rightarrow \exists \xi \in (a,b): f'(\xi) = \frac{f(b) - f(a)}{b - a}$

**Landau:** $I \subseteq \mathbb{R}$ off. Int., $f, g: I \rightarrow \mathbb{R}, c \in \overline{I}/ c = \pm \infty:$ **1)** $f(x) = o(g(x))$ für $x \rightarrow c$ $:\Leftrightarrow \lim_{x \rightarrow c} \frac{f(x)}{f(x)} = 0$. **2)** $f(x) = O(g(x))$ für $x \rightarrow c$ $:\Leftrightarrow \lim_{x \rightarrow c} \frac{f(x)}{g(x)} < \infty \Leftrightarrow \exists K > 0: \forall (x_n)$ mit $\lim_{n\rightarrow\infty} x_n = c: \exists n_0 \in \mathbb{N}: \forall n > n_0: |f(x_n)| \leq K \cdot |(g(x_n)|$.

Störung: $f(c+h) = f(c) + f'(c) \cdot h + o(|h|)$

**L'Hospital: **
$f, g$ st diffbar, $f(c) = f(g) = 0$, $\lim_{x \rightarrow c} \frac{f'(x)}{g'(x)}$ ex $\Rightarrow$ $\lim_{x \rightarrow c} \frac{f(x)}{g(x)} = \lim_{x \rightarrow c} \frac{f'(x)}{g'(x)}$.

**Taylorpolynom:**
$T_nf(x; c) := \sum_{k=0}^{n} \frac{f^{(k)}(c)}{k!}(x - c)^k$

**S v Taylor:** $I \subset \mathbb{R}$ off Int, $c \in I, n \in \mathbb{N}, f \in C^{n+1}(I)$ $\Rightarrow \forall x \in I: f(x) - T_nf(x;c) = R_{n+1}(x)$ mit $R_{n+1}(x) = \frac{1}{n!}\int_{c}^{x} (x-t)^n f^{(n+1)}(t) dt = \frac{f^{(n+1)}(\xi)}{(n+1)!} (x-c)^{n+1}$ mit $c \leq \xi \leq x$. Insb $R_{n+1}(x) = O((x-c)^{n+1})$ f $x \rightarrow c$.







# Integration

$f: [a,b] \rightarrow \mathbb{R}$ beschr **1)** $f$ stet $\Rightarrow f$ intbar **2)** $f$ mon $\Rightarrow f$ intbar **3)** ggf. stückweise stet/mon $\Rightarrow f$ stückw intbar.

**Eigs:** **1)** Linearität **2)** Monotnoie **3)** Integrationsint aufteilen

**Mittelwerts der Int.rech.:** $f:[a,b] \rightarrow \mathbb{R}$ stetig: $\exists \xi \in [a, b]: \int_{a}^{b} f(x) \,dx = f(\xi)(b-a)$

**Part Int:** $\int_a^b f(x)g'(x) dx = [f(x)g(x)]_a^b - \int_a^b f'(x)g(x) dx$

**Subst:** $\int_a^b f(g(x)) \cdot g'(x) dx = \int_{g(a)}^{g(b)} f(y) dy$

**Log Integr:** $\int_a^b \frac{g'(x)}{g(x)} dx = [\ln(|g(x)|]^{f(b)}_{f(a)}$ (Subst. $u := g(x)$).

**Trig Sub:** **1)** $\sqrt{a^2 - x^2}$; $x =: a\sin(u)$; $1 - \sin^2(u) = \cos^2(u)$.
**2)** $\sqrt{a^2 + x^2}$; $x =: a\tan(u)$; $1 + \tan^2(u) = \frac{1}{\cos^2(u)}$.
**3)** $\sqrt{x^2 - a^2}$; $x =: a\frac{1}{\cos(u)}$; $\frac{1}{\cos^2(u)} - 1 =  \tan^2(u)$. **2alt)** $\sqrt{a^2 + x^2}$; $x =: a\sinh(u)$; $\sinh^2(x) - \cosh^2(x) = 1$

**Partbruchzerl** 1) Ggf. Polynomdivision 2) Nenner in Linfak (ggf. mit Polydiv) 3) Gleichung der Form $\frac{p(x)}{q(x)} = \frac{A}{x-x_1} + \frac{B}{x-x_2} + ...$ 4) $q(x)$ auf andere Seite mult 5) Nullstellen einsetzen u nach $A$, $B$, auflösen.

**Uneig. Integr:** Integrale über GW definiert; wenn beidseitig irgendwo(!) dazwischen aufspalten und getrennt, dann add. $f$ abs intbar wenn $|f|$ intbar $\Rightarrow$ $f$ intbar.

**Maj.krit f Int:** $f, g: [a,b] \rightarrow \mathbb{R}$ mit Eig 1) $f$ über jedem Int $[a, y]$ mit $a < y < b$ $\land$ 2) $\forall x \in [a, b): |f(x)| \leq g(x)$ $\land$ 3) $g$ über $[a, b)$ uneig intbar. $\Rightarrow$ $f$ über $[a, b)$ abs intbar.

**Folg:** **1)** $f:(a,b] \rightarrow \mathbb{R}$ über jedem Int $[c,b]$ mit $a < c < b$ intbar und $f(x) = O(1\frac{1}{|x-a|^s})$ für $x \rightarrow a$ mit $s \in [0, 1)$ $\Rightarrow$ $f$ über $(a, b]$ uneig intbar. **2)** $f:[a,\infty) \rightarrow \mathbb{R}$ über jedem Int $[a,b]$ mit $a < b$ intbar und $f(x) = O(1\frac{1}{x^s})$ für $x \rightarrow \infty$ mit $s > 1$ $\Rightarrow$ $f$ über $[a, \infty)$ uneig intbar.

**Int.vgl.krit.:** s. Reihen

- $\int_{1}^{\infty} \frac{1}{x^s} \,dx = \frac{1}{s-1}$ für $s > 1$ und $\infty$ für $s \leq 1$.
- $\int_{0}^{1} \frac{1}{x^s} \,dx = \frac{1}{1-s}$ für $s < 1$ und $\infty$ für $s \geq 1$.
- $\forall \alpha > 0: \int_{0}^{\infty} e^{-\alpha x} \,dx = \frac{1}{\alpha}$
- $\int_{-\infty}^{\infty} \frac{1}{\sqrt{1+x^4}} \,dx$ ex
- $\int_{-1}^{1} \frac{1}{\sqrt{1-x^2}} \,dx$ ex
- $\int_{-\infty}^{\infty} x \,dx$ ex nicht


# Kurven

Kurve ist Abb $\mathbb{R} \rightarrow \mathbb{R}^n$. Bild $k(I) = \{k(t) : t \in I\}$ **Spur**.

**Ableitung** $k'(t) = (k_1'(t),...,k_n'(t))$ heißt auch *Tangentialvektor*/*Geschw.vektor*. $||k'(t)||$ *Geschw* zur Zeit $t$.

**Regulär** wenn $k'(t) \neq 0$ ($\frac{k'(t)}{||k'(t)||_2}$ Tan.einh.vekt).
**Singulär** wenn $k'(t) = 0$

**Schnittwinkel** bei Doppelpunkt: $\cos(\alpha) = \frac{>k'(t_0), l'(t_1)>}{||k'(t_0)|| \cdot ||l'(t_1)||}$

**rektifizierbar:** $\Leftrightarrow \{\sum_{k=1}^{N} ||\gamma(t_k) - \gamma(t_{k-1})||_2: a = t_0 < ... < t_N = b$ Unterteilung von $[a, b]\}$ beschränkt. $L(k) := \sup_{[a,b] \subset I} L(k|_{[a,b]}) < \infty$

**Bogenlänge** $L(k) = \int_a^b |k'(t)| dt$

**Parametertransformation** eine bijektive Funktion f. Setzt man sie in eine Kurve ein kriegt man eine andere Kurve mit gleicher Spur. Bogenlänge bleibt gleich

**Krümmung** für 2D Kurven

$\kappa(t) = \frac{x'(t)y''(t) - y'(t)x''(t)}{(x'(t)^2 + y'(t)^2)^{\frac{3}{2}}}$

**Kurven:**

- Verb.str. zw. $v$ u $w$: $k:[0,1] \rightarrow \mathbb{R}^n, k(t) := v + t(w-v)$
- Einheitskreis: $k: [0, 2\pi] \rightarrow \mathbb{R}^2, k(t) := (\cos(t), \sin(t))$, bzw. $k_2: [0, \pi] \rightarrow \mathbb{R}^2, k(t) := (\cos(2t), \sin(2t))$
- Ellipse mit Halbachsen $a$ und $b$: $k: [0, 2\pi] \rightarrow \mathbb{R}^2, k(t) := (a\cos(t), b\sin(t))$
- Neilsche Parabel: $k: \mathbb{R} \rightarrow \mathbb{R}^2, k(t) := (t^2, t^3)$
- Kurve mit Graph der Betr.funk als Spur: $k: \mathbb{R} \rightarrow \mathbb{R}^2, k(t) := (t, \vert t \vert)$ bzw. $k_2(t) := (sgn(t) t^2, t^2)$
- Schraubenlinie: $k: \mathbb{R} \rightarrow \mathbb{R}^3, k(t) := (r\cos(t), r\sin(t), t)$



# Mehrdimensionale Differentialrechnung

**Partielle Ableitung** $\partial_j f(x) :=$ *Ableitung nach Variable x*

**Gradient** Ableitungen nach allen Variablen untereinander schreiben

$\bigtriangledown f(x) = \begin{pmatrix} \partial_1f(x) \\ \vdots \\ \partial_nf(x)\end{pmatrix}$

**Hessematrix** Ableitungen des Gradienten nach Rechtes nebeneinadner schreiben

$\bigtriangledown^2 f(x) = \begin{pmatrix} \partial_1\partial_1f(x) & ... & \partial_1\partial_nf \\ \vdots & & \vdots \\ \partial_n\partial_1f(x) & ... & \partial_n\partial_nf(x) \end{pmatrix}$

**Richtungsableitung** In Richtung $v$ mit Gradient lösbar: $\partial_vf(x) = \bigtriangledown f(x) \circ v$

**Extrema**

*Kritischer Punk* $\bigtriangledown f(x) = 0$

*Maximum/Minimum/Sattelpunk* Eigenwerte $\lambda$ von $\bigtriangledown^2 f(x)$ bestimmen.

$\lambda$ bestimmen durch $\chi_A = det(A - \lambda I) = 0$

- Alle $\lambda > 0$ : Minimum
- Alle $\lambda < 0$ : Maximum
- $\exists \lambda > 0 \land \exists \lambda < 0$: Sattel

## In meherere Dimensionen

**Jacobi-Matrix** wenn $F(x) = \begin{pmatrix} f_1(x) \\ \vdots \\ f_m(x) \end{pmatrix}$

$DF(c) := \begin{pmatrix} \partial_1f_1(x) & ... & \partial_nf_1(x) \\ \vdots & & \vdots \\ \partial_1f_m(x) & ... & \partial_nf_m(x) \end{pmatrix}$

**Kettenregel** $D(G \circ F)(c) = DG(F(c)) \circ DF(c)$



# Mehrdimensionale Integralrechnung

Geschachtelte Integrale wertet man nacheinander aus, Variablen von außen werden als konstant angesehen

**Satz von Fubini** Mehrere Integrale sind vertauschbar

$\int_{a_1}^{b_1}\int_{a_2}^{b_2}f(x,y) dy dx = \int_{a_2}^{b_2}\int_{a_1}^{b_1}f(x,y) dx dy$

**Für Bereiche** $\int_a^b \int_{f(a)}^{f(b)} f(x,y) dy dx$



# Differentialgleichungen

<!--- **Satz von Picard Lindelöf** TODO -->

$y'(t) = f(t) \cdot g(y(t))$

- Bestimme $F(t)$ von $f(t)$
- Bestimme $G(t)$ von $\frac{1}{g(t)}$
- Löse $G(y(t)) = F(t) + c$ für $c \in \mathbb{R}$

$y'(t) + a(t) \cdot y(t) = 0$

- Bestimme $A(t)$ von $a(t)$
- Lösung ist $y(t) = ce^{-A(t)}$ für $c \in \mathbb{R}$

$y'(t) + a(t) \cdot y(t) = f(t)$

- Bestimme $A(t)$ von $a(t)$
- Bestimme $B(t)$ von $e^{A(t)} \cdot f(t)$
- Lösung ist $y(t) = e^{-A(t)} \cdot (c + B(t))$ für $c \in \mathbb{R}$

$y''(t) + ay'(t) + by(t) = 0$

- Falls $a^2 > 4b$
	- Bestimme $\lambda_{1,2}$ in $\lambda^2 + a\lambda + b = 0$
	- $y(t) = c_1e^{\lambda_1t} + c_2e^{\lambda_2t}$ mit $c_{1,2} \in \mathbb{R}$

- Falls $a^2 = 4b$ *(Shortcut von davor)*
	- $\lambda = - \frac{a}{2}$
	- $y(t) = (c_1 + c_2) e^{\lambda t}$ mit $c_{1,2} \in \mathbb{R}$

- Falls $a^2 < 4b$
	- $\omega = \sqrt{b-(\frac{a}{2})^2}$
	- $y(t) = (c_1 \cos(\omega t) + c_2 \sin(\omega t)) e^{-\frac{a}{2}t}$

$y''(t) + ay'(t) + by(t) = p(t)$ *(polynom)*

- Bestimme Lösung zu $y_h'' + ay_h' + by_h = 0$
- Bestimme Lösung zu $y_p'' + ay_p' + by_p = p(t)$ mit $y_p(t) = \begin{cases} q(t) & b \neq 0 \\ tq(t) & a \neq 0 \land b = 0 \\ t^2q(t) & a = 0 \land b = 0 \end{cases}$ und \ $q(t) = a_nt^n + ... + a_0$
- Lösung ist $y(t) = y_h(t) + y_p(t)$ für $c_{1,2} \in \mathbb{R}$

$y'(t) = Ay(t)$

- Berechne Eigenwerte $\lambda_1,...,\lambda_n$ von $A$
- Berechne die zugehörigen Eigenvektoren $v_1,...,v_k$ mit der Formel $(A-\lambda_1I_n)v_i=0$
- Lösung ist $y(t) = c_1e^{\lambda_1t}v_1 + ... + c_ke^{\lambda_kt}v_k$
